const mqtt = require('mqtt')
const say = require('say')

if(process.argv.length < 4) {
    console.error('Invalid number of arguments, usage:\n\n\tnode app.js localhost:1883 test');
    process.exit(-1);
}

const host = process.argv[2];
const topic = process.argv[3];
const client = mqtt.connect(`mqtt://${host}`);

client.on('connect', function () {
    console.log(`Connected to broker in host: ${host}`);

    client.subscribe(topic, function (err) {
        if (!err) {
            console.log(`Subscribed to topic: ${topic}`);
        }
        else {
            console.log(`Could not subscribe to topic: ${topic}. ${err}`);
        }
    })
})

client.on('message', function (channel, message) {
    if(channel == topic) {
        say.speak(message);
        console.log(`Voiced message: ${message}`);
    }
})