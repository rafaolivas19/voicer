## Description

TTS Agent. Subscribes to a MQTT broker and reproduces any message received by the broker with the computers default voice.

## Windows Instalation 

The Windows recomended installation is through NSSM.

### Pre requisites

1. Node JS installed with NPM package manager.
1. [Git](https://git-scm.com/downloads) installed.
1. [NSSM](https://nssm.cc/download) installed.
1. (**Optional**) Add an enviroment variable to NSSM so you can use the `nssm.exe` from terminal without the need to specify the full path.

### Installation

1. Clone this repository (put it in a place where it won't be moved).
1. Open a terminal (**with admin privileges**) in the cloned repo and download all npm dependencies with: `npm install`.
1. Install as service using the same terminal with: `nssm install Voicer '${NODE_HOME}' '${APP_PATH}' '${MQTT_HOST}' '${MQTT_TOPIC}'`, e.g. `nssm install Voicer 'C:\Program Files\nodejs\node.exe' 'C:\Users\rafao\Documents\Voicer\app.js' 'localhost:1883' 'voicer'`.

### Notes

- After installation you may want to set the service to start automatically, you can do this from `Services -> Voicer -> Right Click -> Properties -> Startup Type -> Automatic (or Automatic with delay)`.

## Linux installation

*Coming soon...*